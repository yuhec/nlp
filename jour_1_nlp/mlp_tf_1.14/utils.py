import tf_pipeline
import tensorflow.compat.v1 as tf
import pandas as pd
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
import os

def load_data(dir:str):
    """
    Process data into feature list and corresponding labels

    :param dir: dataset directory
    :return: the feature matrix of the whole dataset and the list of associated labels
    """
    # open each file of the dataset
    file_list = os.listdir(dir)
    txt_list = []

    for file in file_list:
        # encoding='utf-8' for windows
        file_path = os.path.join(dir, file)
        if file == "Scores.txt":
            # load scores for each file
            scores = pd.read_csv(file_path, sep=";")
            # we need to put the labels in the same order as the list
            file_list2 = file_list
            file_list2.remove("Scores.txt")
            scores.set_index("Fichier", inplace=True)
            scores = scores.reindex(file_list)
            pertinence_list = np.array(scores[["Business_marketing", "ScoreTechno", "ScoreMetier"]], dtype=float)
        else:
            with open(file_path, 'r', encoding='utf-8') as f:
                txt_list.append(f.read())

    # we use a simple way to encode text by the frequency of each word
    cv = CountVectorizer()
    features_list = cv.fit_transform(txt_list).toarray()
    print("Input size is ", features_list.shape[1])
    print("Output classes are ", pertinence_list.shape[1])
    return features_list, pertinence_list


def variable_summaries(var):
    """Attach a lot of summaries to a Tensor (for TensorBoard visualization).

    :param var: variable to summarize
    """
    with tf.name_scope('summaries'):
        mean = tf.reduce_mean(var)
        tf.summary.scalar('mean', mean)
        with tf.name_scope('stddev'):
            stddev = tf.sqrt(tf.reduce_mean(tf.square(var - mean)))
        tf.summary.scalar('stddev', stddev)
        tf.summary.scalar('max', tf.reduce_max(var))
        tf.summary.scalar('min', tf.reduce_min(var))
        tf.summary.histogram('histogram', var)



def add_evaluation_step(result_tensor, ground_truth_tensor):
    """
    Add the operations we need to evaluate the accuracy of our results.

    :param result_tensor: The new final node that produces results.
    :param ground_truth_tensor: The node we feed labels data
    :return: Tuple of (evaluation step, prediction)
    """
    with tf.name_scope('accuracy'):
        with tf.name_scope('correct_prediction'):
            prediction = tf.argmax(result_tensor, 1)
            correct_prediction = tf.equal(
                prediction, tf.argmax(ground_truth_tensor, 1))
        with tf.name_scope('accuracy'):
            evaluation_step = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    tf.summary.scalar('accuracy', evaluation_step)
    return evaluation_step, prediction



def get_batch(feature_list, label_list, validation=False):
    """
    Get a random batch from training set if validation = false, else get a validation batch

    :param feature_list:
    :param label_list:
    :param validation:
    :return:
    """
    if validation :
        batch_features = feature_list[tf_pipeline.TRAINING_SET_SIZE:]
        batch_labels = label_list[tf_pipeline.TRAINING_SET_SIZE:]
    else :
        training_features = feature_list[:tf_pipeline.TRAINING_SET_SIZE]
        training_labels = label_list[:tf_pipeline.TRAINING_SET_SIZE]
        batch_index = np.random.permutation(len(training_labels))[:tf_pipeline.BATCH_SIZE]
        batch_labels = training_labels[batch_index]
        batch_features = training_features[batch_index]
    return batch_features, batch_labels
