import tensorflow.compat.v1 as tf
import utils
import time

SUMMARIES_DIR = "./train_log"
DATASET_DIR = "./data/"
NB_TRAINING_STEP = 1
TRAINING_SET_SIZE = 160
BATCH_SIZE = 100

def my_network():
    # TODO : create network's inputs
    # we need two placeholders, one for input data and one for input labels
    input_size = 11304 # nb de mots différents dans l'ensemble des textes
    output_size = 3 # ?
    hidden_size = 100 # nb de neurones sur la couche cachée
    # Entrée réseau
    text_input = tf.placeholder(tf.float32, shape=[None, input_size]) # None = taille de batch dynamique
    # Entrée loss
    labels_input = tf.placeholder(tf.float32, shape=[None, output_size])


    # TODO : create the weights of the network.
    # You can use tf.Variable for storing them, and tf.truncated_normal for initialisation
    layer_weights = tf.Variable(tf.truncated_normal([input_size, hidden_size])) # Contient tous les poids de tous les neurones


    # TODO : create biases
    # you can use the same initialisation as the variables
    layer_biases = tf.Variable(tf.random.truncated_normal([hidden_size]))
    # Viendra s'additionner aux poids


    # TODO : compute the output
    # output = activation_function(input * weights + bias)
    # you can use tf.sigmoid and tf.matmul here to compute your output
    logits = tf.sigmoid(tf.add(tf.matmul(text_input, layer_weights), layer_biases))
    # Add another layer
    logits = tf.layers.dense(logits, output_size)
    logits = tf.nn.dropout(logits, 0.5)
    # we compute the softmax (tf.nn.softmax) of the output to class probabilities from class values
    predictions = tf.nn.softmax(logits)
    if predictions is None:
        print("Missing network creation")

    # TODO : Add how to train the network
    # TODO : step 1 : compute the error (loss)
    # you can use  tf.nn.softmax_cross_entropy_with_logits to compute the loss for the batch
    # then tf.reduce_mean to get the average loss on the batch for learning
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits_v2(labels=labels_input, logits=logits))

    # TODO : train the network with an optimizer and your loss
    # initialize an optimizer (like tf.train.AdamOPtimizer)
    # then use it to optimize the weights and bias (optimizer.minimize(...))
    optim = tf.train.AdamOptimizer()
    train_op = optim.minimize(loss)

    # These lines add summaries for Tensorboard
    if train_op is not None:
        utils.variable_summaries(layer_weights)
        utils.variable_summaries(layer_biases)
        tf.summary.histogram('logits', logits)
        tf.summary.histogram('predictions', predictions)
        tf.summary.scalar('loss', loss)
    else :
        print("Missing network training operations")

    return (train_op, loss, text_input, labels_input, predictions)


def main():
    # Setup the directory we'll write summaries to for TensorBoard
    if tf.gfile.Exists(SUMMARIES_DIR):
        tf.gfile.DeleteRecursively(SUMMARIES_DIR)
    tf.gfile.MakeDirs(SUMMARIES_DIR)

    # Look at the folder structure, and create lists of all the images.
    feature_list, label_list = utils.load_data(DATASET_DIR)

    with tf.Session() as sess:
        # Create the operations we need to evaluate the accuracy of our new layer.
        train_op, loss, text_input, labels_input, output_tensor = my_network()

        # Merge all the summaries and write them out to the summaries_dir
        merged_op = tf.summary.merge_all()
        train_writer = tf.summary.FileWriter(SUMMARIES_DIR + '/train/'+str(time.time()), sess.graph)

        # Set up all our weights to their initial default values.
        init = tf.global_variables_initializer()
        sess.run(init)

        train_summary = None
        # Run the training for as many batches as requested
        for i in range(NB_TRAINING_STEP):
            # get batch from training dataset
            batch_features, batch_labels = utils.get_batch(feature_list, label_list)

            # TODO : train the network on each batch
            train_summary, _, your_outputs = sess.run([merged_op, train_op, output_tensor], feed_dict={text_input: batch_features, labels_input: batch_labels})
            # we log the training
            if train_summary is not None:
                train_writer.add_summary(train_summary, i)

        if train_summary is None:
            print("Missing training for each batch")

        # we evaluate and display the accuracy on the training and the validation dataset
        if output_tensor is not None:
            evaluation_step = utils.add_evaluation_step(output_tensor, labels_input)
            # Run final evaluation
            full_training_dataset = feature_list[:TRAINING_SET_SIZE]
            full_training_labels = label_list[:TRAINING_SET_SIZE]
            final_training_accuracy = sess.run([evaluation_step], feed_dict={text_input: full_training_dataset,
                                                                               labels_input: full_training_labels})
            print('Final training accuracy', final_training_accuracy[0][0])
            validation_inputs, validation_labels = utils.get_batch(feature_list, label_list, validation = True)
            final_validation_accuracy = sess.run([evaluation_step],feed_dict = {text_input: validation_inputs,
                                                                                   labels_input : validation_labels})
            print('Final validation accuracy', final_validation_accuracy[0][0])


if __name__ == '__main__':
    main()