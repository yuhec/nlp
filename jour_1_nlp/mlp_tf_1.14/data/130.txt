The Most Powerful Information Tool
the Farm Has Ever Seen
SLANTRANGE systems create valuable new agricultural metrics through an innovative combination of remote sensing and in-field analytics.
Vital New Information
Do I need to replant?
What is the extent of my weed problem?
Do I have a pest issue?
What is my nutrient status?
Where is my irrigation failing?
Where are my yields trending?
SLANTRANGE provides new tools to answer very specific questions.
Accurate
Confidence in your decisions requires confidence in your data. SLANTRANGE has pioneered and patented new techniques to ensure data accuracy and consistency, so trends can be characterized and forecasts developed.
Immediate & On-Demand
Information is perishable, especially for growers. SLANTRANGE will take you from need-to-know to knowledge in less than an hour.
Scalable Anywhere
No network access?
No problem.
SLANTRANGE processes data at the point of collection. Instant results in the absence of infrastructure, on any drone, from Napa to Namibia.
The Most Powerful Information Tool
the Farm Has Ever Seen June 16th, 2017mritter
headlines
Qualcomm Snapdragon Flight is an ideal platform to provide the processing power for SLANTRANGE’s on-board analytics, and the 3p showcases how our connectivity and compute technologies can not only contribute to the growing commercial drone industry but can help the farming industry.
Qualcomm
A patent has just been issued to SLANTRANGE covering foundational technologies for aerial crop inspection.
Commercial UAV News
SLANTRANGE has made waves in the agriculture community with their calibrated multispectral sensors and rapid off-line image processing.
DroneDeploy
1 of the 50 most promising startups from around the world.
Bloomberg
Winner of the Innovative World Technologies category in the 2015 SXSW Accelerator competition.
Forbes
What makes SLANTRANGE stand out is its ability to process this data and generate actionable data products in only about 5 minutes.
Gartner
SLANTRANGE makes precision agriculture even more precise.
UAS Magazine
Internet access and bandwidth are huge problems for other drone mapping systems.  SLANTRANGE doesn’t require internet at all and still produces maps within 15 minutes after efficient flights. This means that farmers and agronomists have a map ready for their use, for ground-truthing before they leave the field.
Markus Weber, LandView, Canada
Hands down, you guys have the best support of anyone I’ve ever dealt with in the entire drone industry.
Sam Thier, Precision Crop Imaging, USA
The flexibility and customer service I have received through SLANTRANGE has been unmatched. The progressiveness this company shows toward collaboration and integration are true signs of their success and dedication to agriculture and the end user.
Matt Westerhaus, Midwest Unmanned Aerial Services, USA
The new system is working flawlessly! We’ve been doing a lot of test flights lately. We’re more than pleased with the overall system.
Austin Rice, Yield Tech Aerial Services, USA
The GSD is great at 4.8 cm at 400 ft, which is twice as good as MicaSense RedEdge.
Louis Wasson, Mississippi State University Extension, USA
More acres covered, more actionable data, less headaches. This system just works.
Chris Beerman, Aerial Agronomics, USA
SLANTRANGE has provided us with outstanding service and product support since Day 1. Any issues or questions were addressed immediately and personally. They are a great group to work with.
Landon Sarver, Sarvus Unmanned Systems, Canada
The 2p sensor from SLANTRANGE is the best scouting tool of all the sensors I’ve worked with.  Downloading and processing the data in the field makes it the most practical instrument available.
Marinus Le Roux, AgriSig, South Africa
Reliability is a word that is essential for companies to succeed. SLANTRANGE is the epitome of a company you can rely on. Communication with their team is flawless. Thank you SLANTRANGE for always innovating and for being ahead of the game not only in your tech but also your service!!!
Darryl Anunciado, ActionDrone, USA
“Hi SLANTRANGE friends, we are very happy with your sensor, doing about 600 acres a day on potatoes and corn fields.”
Luis Perez, Productora Agricola El Encanto, Mexico
Headlines June 16th, 2017mritter
Technology for Entirely New Types of Information
Accurate. Versatile. Efficient. Anywhere.
“Slow and low” aircraft, like the small unmanned aerial systems (sUAS) covered in the FAA’s new Part 107 rule, provide a platform for entirely new types of agricultural measurement and information – information that cannot be captured by satellites, manned aircraft, or ground-based systems. But delivering on that potential requires innovation.
Measurements must be accurate to be trusted by growers, or to have any relevance to downstream data models.  That means important vegetation signals must be separated from the background clutter and normalized for changing environmental conditions.
Analytical systems must be versatile enough to address the very specific information needs of growers across an incredibly diverse industry.
The cost of data collection, processing, and information delivery must be drastically reduced so that the benefits of these new types of information can accrue even to smallest farmers in the most remote regions of the world.
SLANTRANGE is addressing each of those challenges with technical innovations in remote sensing hardware systems, new computer vision and artificial intelligence analytics, and in the network architectures for how data is collected, processed, and distributed.
Simply put, we’re working hard to enable growers to produce more with less. But if you’re interested in learning more, just click the link below.
