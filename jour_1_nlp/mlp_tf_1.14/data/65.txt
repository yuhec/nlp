tweet
Toronto-based Deveron UAS Corp. , a drone-based data provider to the agricultural industry in North America, has announced a multiyear commitment to working with Thompsons Ltd. , a farming retailer that has been in business since 1924.
Under the contract, which is expected to run through 2018, Deveron will provide remote sensing data to Thompsons’ growers.
Thompsons Ltd., headquartered in Ontario, is a wholly owned subsidiary of Lansing Trade Group LLC and The Andersons Inc., agribusinesses based in Kansas and Ohio, respectively.
The focus of Thompsons’ “advanced agronomy” product is to help growers achieve more profit per acre. Now, says Deveron, drone data and on-demand imagery will be key ingredients to help drive accurate field scouting, near-real-time crop maps, management zone creation for variable-rate-input prescriptions and detailed yield maps for growers.
“We are excited to be partnering with Deveron UAS to help us continue to more precisely document the variability that every field has,” states Jevin Vyn, agronomy solutions specialist at Thompsons.
TAGS
