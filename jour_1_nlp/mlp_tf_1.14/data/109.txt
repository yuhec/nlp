You are here: Home / Agriculture Drones
Drones for Agriculture
Continuously monitor the health of your crops
Save money - Apply fertilizers & pesticides only when and where needed
Improve crop yields – Oversee your operations and identify problems early
Full Solution Data-Driven Farm Management
Event 38 provides complete solutions for farm management using drones, infrared cameras and powerful image analysis software. We have been building mapping drones and sensors since 2011 and have partnered with corn, soybean and wheat growers all over the world to develop a system tailored to your needs. Our agriculture packages include everything needed to collect, analyze and incorporate drone data into your management practices.
• Continuously monitor the health of your crop – Detect nitrogen deficiency, disease, drought, pest infestation, high salinity, and more.
• Save money – Apply fertilizers, pesticides, and herbicides only where they are needed.
• Improve crop yields – Oversee your farming operations and identify problems before they spread.
Our drones are rugged workhorses, proven in thousands of flights in the real life conditions to meet the demands of today’s data driven farmer.
Fly
Event 38 drones fly completely autonomously, covering up to 960 acres in a single flight, and land automatically. Capture all the aerial data you need at once, and see down to the leaves on every plant. Easy to use – plan a mission once and the drone remembers how to fly each field and where to land. All of our drones are built durably to withstand hundreds of flights and all-terrain landings
Analyze
Our Drone Data Management System™ takes the data from your flight to produce high resolution georeferenced maps, growth statistics, and management zones based on health and elevation contours. Results can be viewed online, on mobile, shared or downloaded. DDMS™ management zones are compatible with Ag Leader SMS™ Software
Act
Make data-driven management decisions on equipment, seeding, inputs, harvesting and day-to-day operations of your farm
E38-Field Navigator App showing NDVI map of corn at V12
Next Level Crop Scouting
Event 38 drones and sensors bring crop scouting to the next level by putting the entire field at a farmer’s fingertips. Not only does the perspective lend itself to quick analysis, it shows the larger field-scale trends that can go unseen at ground level. Our specially designed NDVI sensor collects infrared light to power crop vigor analyses like NDVI and DVI . These colorized health maps help highlight problem areas as they emerge, enabling quick action to correct operational issues.
When you spot an area of interest, use our E38-Field Navigator App that puts a map of your field on any phone or tablet and continuously pinpoints your location as you scout on the ground. Record your findings by saving notes back to the map.
Mark locations in your fields for further inspection
_
Follow your current location on your phone or tablet to navigate large fields
Quantify Observations
The Drone Data Management System™ provides all the tools necessary to discover and quantify good and bad aspects of your operation. We’ve built DDMS™ to be compatible with leading precision agriculture tools like Ag Leader SMS so you can use your drone for more than just taking pictures. Several apps export results that can be used to assist in making prescription maps and managing water.
See our growing list of applications that are available now:
NDVI Vegetation Health
Using infrared light and visible light, NDVI quantifies relative plant health and are an early indicator of plant stress.
Event 38 Unmanned Systems
Emergence Uniformity
See how different areas of a field perform, and receive a report containing the crop cover of each grid section.
Event 38 Unmanned Systems
Management Zones
Divides each field into sections based on plant health. Zones can be used to allocate fertilizers and soil amendments, and to avoid wasting them where they won’t make a difference.
Event 38 Unmanned Systems
Water Drainage Map
Find areas of your fields where water is likely to flow and pick appropriate seed varieties and fertilizer application rates.
Event 38 Unmanned Systems
DVI Vegetation Health
Using infrared light and visible light, DVI quantifies relative plant health and are an early indicator of plant stress. DVI often performs better than NDVI when shadows are present.
Event 38 Unmanned Systems
Create centimeter-precision 3D elevation models of your farm.
Event 38 Unmanned Systems
Vegetation Area Index
Calculate the fraction of your fields covered by vegetation to track growth and compare emergence performance between different fields.
Event 38 Unmanned Systems
Health Management Zones, Elevation Contours, and Water Drainage Maps are compatible with SMS
Event 38 Unmanned Systems
Google Earth Export
Download maps of your fields in KMZ format for easy viewing and sharing with Google Maps.
Event 38 Unmanned Systems
Export elevation contours to see how terrain affects growth and yield.
Event 38 Unmanned Systems
Your Complete Solution for Agriculture
Improve yields, save money, manage operations
Everything you need for $4659
