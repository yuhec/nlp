Agricultural Drones Market Shares, Strategies, and Forecasts, Worldwide, 2016 to 2022
Table of Contents
Agricultural Drone Executive Summary 27
Agricultural Drone Market Driving Forces 27
Agricultural Drones For Crop Surveillance 31
Drone Agricultural Inspection and Planting Aerial Systems Market Shares 33
Agricultural Drone Market Forecasts 35
1. Agricultural Drones: Market Description and Market Dynamics 37
1.1 Venture Investment In Agricultural Drones 37
1.1.1 Agricultural Industrialization 38
1.1.2 Transparency Benefits Of IoT and Digital Farming 38
1.2 Smart Drones: Commercial Unmanned Aerial Systems (UAS) Description 40
1.3 Drone Enhanced Capability and Payloads 41
1.4 Nano Drones Applications 43
1.4.1 Drone Miniaturization 44
1.5 Follow Me Drones 45
1.5.1 US FAA Commercial Drone Permits 46
1.6 Unmanned Aerial Systems Payloads 48
1.6.1 Composites Key to Utility 48
1.6.2 Unmanned Aerial Systems (UAS) Agriculture Mapping 49
1.7 Californian Wine Industry Uses Drones to Refine Sampling of the Vineyard51
1.8 Agri-Informatics 51
1.8.1 Drones Geo-Tag Images And Wirelessly Transmit 53
1.8.2 Drones Used in Japan for Agriculture 54
2. Agricultural Drone Market Shares and Forecasts 58
2.1 Agricultural Drone Market Driving Forces 58
2.1.1 Agricultural Drones For Crop Surveillance 62
2.2 Drone Agricultural Inspection and Planting Aerial Systems Market Shares 64
2.2.1 Agriculture Commercial Drone Market Vertical Growth: 68
2.2.2 Yamaha RMAX 70
2.2.3 DJI Agricultural Spraying Drone 73
2.2.4 Prox Dynamics PD-100 Black Hornet 74
2.2.5 Draganfly Draganflyer X4-P 75
2.2.6 AscTec Firefly 76
2.2.8 AgEagle Aerial Systems Agricultural Crop Map Functions 78
2.3 Agricultural Drone Market Forecasts 80
2.3.1 Agricultural Drone Unit Analysis and Unit Forecasts 83
2.3.2 Agricultural Drone Unit Shipments 85
2.3.1 Agriculture Surveying Via Drones 88
2.3.2 Drone Agricultural Surveying 89
2.3.3 Agricultural Crop Dusting Drones 90
2.3.4 Agricultural Drone Trends 91
2.3.5 Agricultural Drones Provide Farmers With Detailed Views 93
2.4 Drone Shipments 94
2.4.1 Drone Market Shares and Sector Forecasts 100
2.4.2 Drone Market Forecasts 102
2.5 Agricultural Drone Prices 107
2.6 Drone Systems by Application 108
2.7 Agricultural Drone Regional Market Analysis 109
2.7.1 Smart Drone Commercial (UAV) Industry Regional Summary 112
2.7.2 U.S Accounts for 73 Percent of The Worldwide Research, Development, Test, And Evaluation (RDT&E) Spending On Smart Drone Technology 113
2.7.3 U.S. State Department Drone Export Guidelines 115
2.7.4 Canada 115
2.7.6 UK Trade in Drones 117
2.7.7 Drones for the Netherlands 117
2.7.8 Japan 118
2.7.9 Sony Drone Services 121
2.7.10 Japanese Drone Works Inside the Nuclear Power Plant 123
2.7.11 China 125
2.7.12 Chinese Smog-Fighting Drones That Spray Chemicals To Capture Air Pollution 127
2.7.13 China Desires Exports, Steps Up Research In Drones 129
2.7.14 Chinese Commercial Drones 132
2.7.15 Singapore 133
2.7.18 Expansion of US Drone Base in Africa 136
2.7.19 Ethiopia 138
2.7.23 Australian Research on Agricultural Drones 140
3. Agricultural Drone Product Description 143
3.1 Yamaha Crop Dusting Drones 143
3.1.1 Yahama 148
3.1.3 Yamaha Unmanned Helicopters For Industrial And Research Applications 153
3.1.4 Yamaha Rmax Helicopter Agricultural Drone For Crop Spraying 154
3.2 AeroVironment 157
3.2.1 AeroVironment: RQ-11B Raven for Agriculture 157
3.2.2 AeroVironment: RQ-11B Raven 158
3.2.3 AeroVironment Raven Specifications 160
3.2.4 AeroVironment RQ-20A Puma AE 165
3.2.5 AeroVironement Ground Control System 167
3.2.6 BP and AeroVironment FAA-Approved, Commercial Drone Operations Presage Agricultural Applications 168
3.2.7 AeroVironment Integrated LiDAR Sensor Payload 173
3.2.8 AeroVironment AV’s Family of Small UAS 175
3.3 DJI Inspire 1 176
3.3.1 DJI Technology Chinese Crop Spraying Drone 177
3.3.2 DJI Technology Turning to the Rural Market 181
3.3.3 DJI Phantom 183
3.4.1 3D Robotics Launches Line of Mapping Drones 191
3.4.2 3D Robotics 193
3.5.1 FT Sistemas Drone Applications 197
3.6 AgEagle 200
3.6.1 AgEagle Aerial Systems 202
3.7 Raven Industries 203
3.7.1 Raven and AgEagle Partner on UAS Solutions for Agriculture 203
3.8 HoneyComb AgDrones 204
3.11 PrecisionHawk Lancaster Drone Monitors Livestock Temperature To Check For Fever 214
4. Agricultural Drone Research and Technology 216
4.1 Agricultural Research and Technology 216
4.2 Regulations in the Agricultural Drone Industry 217
4.2.1 Drone Regulation Exemptions 217
4.2.2 FAA Plans Final Regulation on Commercial Drone Use by Mid-2016 219
4.2.3 FAA Approved Drone Projects 219
4.3 Agricultural Drone Sensors & Capabilities 229
5. Agricultural Company Profiles 230
5.1 3D Robotics 230
5.1.1 3D Robotics Acquisition of Sifteo 230
5.2 AeroVironment 231
5.2.1 AeroVironment Revenue 2015 232
5.3 AgEagle 236
5.5.1 Airware Components of its Aerial Information Platform 244
5.5.2 Airware’s AIP Business Model 245
5.5.3 Airware Investment from Intel Capital 245
5.6 Aviation Industry Corporation of China (AVIC) 246
5.7 China Aerospace CASC Space Technology 247
5.7.1 China Aerospace CASC Revenue 247
5.8 Deveron Resources Ltd 248
5.8.1 Eagle Scout Drone Imaging 249
5.9 DJI 251
5.9.1 DJI Agricultural Spraying Drone 253
5.9.2 DJI Inspire 1 254
5.9.3 DJI Technology Chinese Crop Spraying Drone 256
5.10 FT Sistemas 259
5.13.3 Parrot Group / senseFly 265
5.13.4 Parrot Group senseFly CTI Certified 265
5.13.5 Parrot Drone First Quarter Sales For 2015 Up 356 Percent 266
5.14 Precision Drone 268
5.17 Tekever Drones To Detect Vine Disease 270
5.17.1 TEKEVER Group drone Project In The Wine-Growing Region Of Minho (North of Portugal) 271
5.17.2 TEKEVER 272
5.18 Topcon Positioning Group 273
5.19 Yahama 274
5.19.1 Yamaha Crop Dusting Drones 278
5.20 Yuneec 279
List of Tables and Figures
Table ES-1 Agricultural Drone Market Driving Forces 28
Table ES-2 Agricultural Drone Market Factors 29
Table ES-3 Agricultural Drone Geo-Referenced Image Advantages 30
Table ES-4 Benefits of Agricultural Drones 32
Figure ES-5 Commercial Drones Took to the Skies First for the Agricultural Industry 33
Figure ES-6 Agricultural Drone Market Shares, Dollars, 2015 34
Figure ES-7 Smart Commercial Drone Agriculture Aerial Systems Forecasts, Dollars, Worldwide, 2016-2022 36
Table 1-1 Ability Of Commercial Drones To Perform Delivery Function in Agriculture 42
Table 1-2 Nano Drones Applications 43
Figure 1-3 DJI Share of FAA Drone Operations Exceptions 47
Table 1-4 Drones Used in Japan for Agriculture Access Benefits 55
Table 1-5 Drone Farming Plot Infrastructure Access Barriers 56
Table 2-1 Agricultural Drone Market Driving Forces 59
Table 2-2 Agricultural Drone Market Factors 60
Table 2-3 Agricultural Drone Geo-Referenced Image Advantages 61
Table 2-4 Benefits of Agricultural Drones 63
Figure 2-5 Commercial Drones Took to the Skies First for the Agricultural Industry 64
Figure 2-6 Agricultural Drone Market Shares, Dollars, 2015 65
Table 2-7 Agricultural Inspection and Planting Drone Unmanned Aerial Systems (UAS) Market Shares, Dollars, Worldwide, 2015 66
Table 2-8 Drone Uses in Agriculture 68
Figure 2-9 Yamaha Helicopter Drone Spraying 71
Figure 2-10 Yamaha RMAX Helicopter Drones 72
Figure 2-11 DJI Agricultural Spraying Drone 73
Figure 2-12 Draganfly Draganflyer X4-P 75
Figure 2-13 AscTec Firefly 76
Table 2-14 Technical Data – AscTec Firefly 76
Table 2-15 AgEagle Aerial Systems Agricultural Crop Map Functions 79
Figure 2-16 Smart Commercial Drone Agriculture Aerial Systems Forecasts, Dollars, Worldwide, 2016-2022 81
Table 2-17 Agricultural Drone Markets Worldwide, 2016-2022 82
Table 2-18 Agricultural Inspection and Planting Smart Drone Aerial Systems (UAS) Market Shares, Units, Worldwide, 2015 83
Figure 2-19 Agricultural Drone Market Forecasts, Units, Worldwide, 2016-2022 86
Table 2-20 Small and Mid-Size Agricultural Drone Systems, Dollars and Units, Worldwide, 2015-2021 87
Figure 2-21 Agriculture Surveying Via Drones 88
Figure 2-22
Table 2-23 Drone Systems Market Share Units, 2015 94
Figure 2-24 Drone Aerial Systems Market Forecasts, Units, Worldwide, 2016-2022 96
Figure 2-25 DJI Overcomes Technological Barriers to Drone Manufacture 98
Figure 2-26 Agriculture, Business, Environmental and Entertainment Use of Drones 99
Figure 2-27 Drone Systems Market Shares, 2015 101
Figure 2-28 Drone Aerial Systems Forecasts, Dollars, Worldwide, 2016-2022 103
Table 2-29 Drone Aerial Systems by Sector, Military, Agriculture, Oil and Gas, Border Patrol, Law Enforcement, Homeland Security, Disaster Response, Package Delivery, Photography, Videography, Dollars, Worldwide, 2016-2022 105
Table 2-30 Drone Systems by Application, Military, Law Enforcement, Homeland Security, and Border Patrol, Agricultural, Package Delivery, Consumer Photo Drones, Utility Infrastructure Inspection, and Mapping, Market Shares, Dollars, Worldwide, 2015 108
Figure 2-31 Drone Aerial Systems (UAS) Regional Market Segments, Dollars, 2015 111
Table 2-32 Drone Aerial Systems (UAS) Regional Market Segments, 2015 112
Figure 2-33 Japanese Hexacopter Smart Commercial Drone 119
Figure 2-34 Sony Commercial Drone 121
Figure 2-35 Drone Model Envisaged For Work Inside The Reactor Buildings At The Crippled Fukushima No. 1 Nuclear Power Plant 123
Figure 2-36 Chinese Vendors Seeks to Sell Drones to China 131
Figure 2-37 Expansion of US Drone Base in Africa 137
Figure 2-38 Australian Research on Agricultural Drones 141
Figure 3-1
Figure 3-2 UC Davis Using Yahama Helicopter Drones For Crop Dusting 145
Figure 3-3 Yamaha Crop Dusting Initiatives 146
Figure 3-4 Yamaha Helicopter Drone Spraying 149
Figure 3-5 Yamaha RMAX Helicopter Drones 150
Figure 3-6 151
Figure 3-7 Yamaha Rmax Drone 152
Figure 3-8 Yamaha Unmanned Helicopters For Industrial And Research Applications 153
Table 3-9 Yamaha Drone Unmanned Helicopters Agricultural Applications 154
Figure 3-10 Yamaha R-Max Agricultural Robot Spraying 155
Figure 3-11 Yamaha Rmax Spraying 156
Figure 3-12 AeroVironment Raven Used for Agriculture 159
Figure 3-13 AeroVironment Agricultural Inspection 160
Figure 3-14 AeroVironment Raven Features 161
Figure 3-15 AeroVironment Raven Specifications 162
Figure 3-16 AeroVironment Raven Flight 163
Figure 3-17 AeroVironment Agricultural Drone Applications 164
Figure 3-18 AeroVironement RQ-20A Puma AE 165
Figure 3-19 AeroVironement Ground Control System 167
Figure 3-20 BP and AeroVironment Drone for Comprehensive GIS Services 169
Table 3-21 AeroVironment BP Services 172
Table 3-22 AeroVironement Inspection of Critical Infrastructure 173
Figure 3-23 AeroVironment Raven 175
Figure 3-24 DJI Inspire 1 176
Figure 3-25 DJI Crop Spraying Agricultural Drone 177
Table 3-26 DJI Agras Agricultural Drone Features 179
Table 3-27 DJI Agras Agricultural Drone Functions 180
Figure 3-28 DJI Technology Farm Drone Can Spray Pesticides Over Up To 4hectares Of Land Every Hour?40 Times More Efficient Than Manual Spraying 182
Figure 3-29 DJI Phantom 183
Figure 3-30 DJI Phantom Series 184
Figure 3-31 DJI Ronin 185
Table 3-32 DJI Agras MG-1 Worldwide Large Farm Challenges 187
Table 3-33 DJI Agras MG-1 Drone Challenges 188
Figure 3-34 3D Robotics 190
Figure 3-35
Figure 3-36 3D Robotics Vineyard Inspection 193
Figure 3-37
Figure 3-38 FT Sistemas Drone Designs 196
Figure 3-39 FT Sistemas Naval Drone Designs 196
Figure 3-40 FT Sistemas RGB Drone Perspectives 197
Figure 3-41 FT Sistemas Drone Applications 198
Figure 3-42 FT Sistemas Brazilian Military Drones 198
Table 3-43 Brazilian Land Force FT100 Mission Targets 199
Figure 3-44 AgEagle 200
Table 3-45 AgEagle Aerial Systems Agricultural Crop Map Functions 201
Figure 3-46 AgEagle Agricultural Drone 203
Figure 3-47 HoneyComb AgDrone 205
Figure 3-48 HoneyComb AgDrone System Controller 206
Figure 3-49 HoneyComb AgDrone Flying 207
Table 3-50 Parrot Sensefly eXom Drone Functions 209
Figure 3-51 Parrot Sensefly Agricultural Drone 210
Figure 3-52 Precision Drone on Farm 211
Figure 3-53 Precision Drone For Agriculture 212
Figure 3-54 Precision Drones Mapping View of Cornfield 213
Figure 3-55 PrecisionHawk Lancaster Drone Monitors Livestock 214
Figure 4-1 US FAA Drone Exceptions by Use Case 218
Table 4-2 AeroVironment Integration Of Sensors And Capabilities For Delivering Actionable Intelligence 229
5.3 AgEagle
Figure 5-1 AgEagle NIR Camera Proprietary Index Filter 238
Figure 5-2 AgEagle Transport Van 239
Figure 5-3 AgEagle Dependable Component Of A Precision Agriculture Program 240
Figure 5-4 Airogistic Drones 241
Table 5-5 Eagle Scout Drone State-Of-The-Art Camera, Sensor And Software Technology Applications 250
Figure 5-6 DJI Phantom 252
Figure 5-7 DJI Agricultural Spraying Drone 253
Figure 5-8 DJI Inspire 1 255
Figure 5-9 DJI Crop Spraying Agricultural Drone 256
Table 5-10 DJI Agras Agricultural Drone Features 258
5.10 FT Sistemas 259
Table 5-11 HoneyComb AgDrones Mission 260
Table 5-12 HUVD Drone Services Industries Targeted 261
Figure 5-13 Parrot Consumer Drone 267
Figure 5-14 Tekevervines 270
Figure 5-15 UC Davis Using Yahama Helicopter Drones For Crop Dusting 275
Figure 5-16 Yamaha Crop Dusting Initiatives 276
Figure 5-17
Extensive Use of Digital Technologies for Diagnostic Applications To Boost Global Optical Imaging Market
The global optical imaging system market is predicted to reach US$ 3 billion by 2024. It is expected to grow at a stable rate and will post a CAGR of more than 8% in the coming years. The growing market infiltration of digital …
Foremost Trends In Global Pet Care Market
The global pet care market is foretold to improve in the forthcoming years as matched to the preceding years and will showcase better sales in various market segments. It is estimated that the global pet care market will grow at a CAGR o…
Improving Standards Of Living To Boost Global Consumer Durables Market
Consumer durables is a cataloguing of consumer goods that are not essential to be bought very often as they are fashioned in a way so as to last for an extended period of time. Profits in the consumer durables sector were most profoundly…
United States Automated Truck Loading System Market Report 2017
Published: 20-Jul-2017        Price: US 3800 Onwards        Pages: 114
In this report, the United States Automated Truck Loading System market is valued at USD XX million in 2016 and is expected to reach USD XX million by the end of 2022, growing at a CAGR of XX% between 2016 and 2022.  Geographically, this report splits the United States market into seven regions: - The West - Southwest - The Middle Atlantic - New England - The South - The Midwest with sales (volume), revenue (value), market share and ......
Self-Driving Cars and Light Trucks: Market Shares, Strategies, and Forecasts, Worldwide, 2017 to 2023
Published: 19-Jul-2017        Price: US 4200 Onwards        Pages: 1005
Worldwide markets are poised to achieve significant growth as self-driving cars and light trucks permit users to implement automated driving. Fleet vehicles from Uber, Google and similar users are likely to be the early adopter groups. Tesla, Mercedes, and Audi are among the vendors with a leadership position in the personal luxury vehicle self-driving car markets, Every car maker seeks to participate in this self-driving personal vehicle market. The ability to do so depends on implem......
Global Machine Vision Market Professional Survey Report 2017
Published: 19-Jul-2017        Price: US 3500 Onwards        Pages: 109
This report studies Machine Vision in Global market, especially in North America, China, Europe, Southeast Asia, Japan and India, with production, revenue, consumption, import and export in these regions, from 2012 to 2016, and forecast to 2022.  This report focuses on top manufacturers in global market, with production, price, revenue and market share for each manufacturer, covering - Cognex Corporation - Basler - Teledyne DALSA - OMRON - Keyence......
Global Automated Truck Loading System Sales Market Report 2017
Published: 14-Jul-2017        Price: US 4000 Onwards        Pages: 119
In this report, the global Automated Truck Loading System market is valued at USD XX million in 2016 and is expected to reach USD XX million by the end of 2022, growing at a CAGR of XX% between 2016 and 2022.  Geographically, this report split global into several key Regions, with sales (K Units), revenue (Million USD), market share and growth rate of Automated Truck Loading System for these regions, from 2012 to 2022 (forecast), covering - United States - China......
Asia-Pacific Automated Container Terminal Market Report 2017
Published: 13-Jul-2017        Price: US 4000 Onwards        Pages: 103
In this report, the Asia-Pacific Automated Container Terminal market is valued at USD XX million in 2016 and is expected to reach USD XX million by the end of 2022, growing at a CAGR of XX% between 2016 and 2022.  Geographically, this report split Asia-Pacific into several key Regions, with sales (Units), revenue (Million USD), market share and growth rate of Automated Container Terminal for these regions, from 2012 to 2022 (forecast), including - China - Japan ......
Asia-Pacific Machine Vision Market Report 2017
Published: 13-Jul-2017        Price: US 4000 Onwards        Pages: 100
In this report, the Asia-Pacific Machine Vision market is valued at USD XX million in 2016 and is expected to reach USD XX million by the end of 2022, growing at a CAGR of XX% between 2016 and 2022.  Geographically, this report split Asia-Pacific into several key Regions, with sales (K Units), revenue (Million USD), market share and growth rate of Machine Vision for these regions, from 2012 to 2022 (forecast), including - China - Japan - South Korea - Ta......
Global (North America, Europe and Asia-Pacific, South America, Middle East and Africa) PLC Market 2017 Forecast to 2022
Published: 12-Jul-2017        Price: US 4880 Onwards        Pages: 116
A programmable logic controller, PLC, is a digital computer used for automation of typically industrial electromechanical processes, such as control of machinery on factory assembly lines, amusement rides, or light fixtures. PLCs are used in many machines, in many industries. PLCs are designed for multiple arrangements of digital and analog inputs and outputs, extended temperature ranges, immunity to electrical noise, and resistance to vibration and impact. Programs to control machine operation ......
Global Automated Truck Loading System Market Research Report 2017
Published: 11-Jul-2017        Price: US 2900 Onwards        Pages: 113
In this report, the global Automated Truck Loading System market is valued at USD XX million in 2016 and is expected to reach USD XX million by the end of 2022, growing at a CAGR of XX% between 2016 and 2022.  Geographically, this report is segmented into several key Regions, with production, consumption, revenue (million USD), market share and growth rate of Automated Truck Loading System in these regions, from 2012 to 2022 (forecast), covering - North America - Euro......
Global Automatic Depalletizer Market Professional Survey Report 2017
Published: 11-Jul-2017        Price: US 3500 Onwards        Pages: 107
This report studies Automatic Depalletizer in Global market, especially in North America, China, Europe, Southeast Asia, Japan and India, with production, revenue, consumption, import and export in these regions, from 2012 to 2016, and forecast to 2022.  This report focuses on top manufacturers in global market, with production, price, revenue and market share for each manufacturer, covering - ABB - Columbia Machine - FANUC - KUKA - Ouellette Mach......
SERVICES
Value for Money
We believe in "optimum utilization of available budget and resources". While servicing our clients' (your) market research requirements, we keep the same approach in focus to help you get the best value for your $$s.
Ever Growing Inventory
Ranging from the smallest feasible / required data (datasheets, data facts, SWOT analysis, company profiles, etc) to full research reports that help you make decisions, our inventory is updated almost on a daily basis with the latest industry reports from domain experts that track more than 5000 niche sectors.
One Stop Solution
Need a custom research report on medical devices market? Require all available business intelligence on 3D printing industry? Exploring F&B sector of a particular country/region? RnRMarketResearch.com is your one-stop-solution to all market intelligence needs. We not only offer custom research and consulting services, we also "bundle" reports to meet your needs and help you fetch the data analysis you require for your business.
Dedicated Client Engagement
Not limited to only "finding" relevant reports for you, our client engagement team dedicates its efforts to understand your "business need" and accordingly maps available research data to help you move forward. Call "your" client engagement executive any time of your day and get your questions answered in order to make the correct business decision.
Saving Time and Efforts
Simply share your research requirement details with us and let us do all the hard work to find required intelligence for you. When you add up our "one stop solution" and "dedicated client engagement" services mentioned above, you obviously know the time and effort saving you do by working with us.
Payment Flexibility
Working with Fortune 500 organizations, we understand the importance of being flexible for payments. Share your payment terms with us and we will surely match up to them to ensure you get access to required business intelligence data without having to wait for the payment to be done.
Post-Purchase Research Support
Have questions after reading a report / datasheet bought through us? Not sure about the methodology used for data available in the research? Talk to us / Share your questions with us and if required, we will connect you with the analyst(s)/author(s) of the report(s) and ensure you get satisfactory answers for the same. Need more data / analysis / report(s) on the topic of your research/project? The RnRMarketResearch.com team is here for you 24X7 to support you with your post-purchase requirements. Subscription Offers & Packages (Get in touch with us for more details - sales@rnrmarketresearch.com / +1 888 391 5441 )
Ad Hoc
Pay - as - you - go / Bucket Subscriptions
Fixed Cost for #of reports
Customize / Personalize as per your needs
