How do you create a strategy for guaranteeing that innovation and creativity flourish in your organization?

When design principles are applied to strategy and innovation the success rate for innovation dramatically improves. Design-led companies such as Apple, Coca-Cola, IBM, Nike, Procter & Gamble and Whirlpool have outperformed the S&P 500 over the past 10 years by an extraordinary 219%, according to a 2014 assessment by the Design Management Institute.

Great design has that “wow” factor that makes products more desirable and services more appealing to users. 

Due to the remarkable success rate of design-led companies, design has evolved beyond making objects. Organizations now want to learn how to think like designers, and apply design principles to the workplace itself. Design thinking is at the core of effective strategy development and organizational change.

“Design-thinking firms stand apart in their willingness to engage in the task of continuously redesigning their business…to create advances in both innovation and efficiency—the combination that produces the most powerful competitive edge.”
—Roger Martin, author of the Design of Business

“Engineering, medicine, business, architecture, and painting are concerned not with the necessary but with the contingent—not how things are but how they might be—in short, with design…Everyone designs who devises courses of action aimed at changing existing situations into preferred ones.”
—Herbert Alexander Simon, Nobel Prize laureate (1969)

You can design the way you lead, manage, create and innovate. The design way of thinking can be applied to systems,  procedures, protocols, and customer/user experiences. The purpose of design, ultimately, in my view, is to improve the quality of life for people and the planet.
What is Design Thinking?

Design Thinking is a methodology used by designers to solve complex problems, and find desirable solutions for clients. A design mindset is not problem-focused, it’s solution focused and action oriented towards creating a preferred future. Design Thinking draws upon logic, imagination, intuition, and systemic reasoning, to explore possibilities of what could be—and to create desired outcomes that benefit the end user (the customer).

“Design thinking can be described as a discipline that uses the designer’s sensibility and methods to match people’s needs with what is technologically feasible and what a viable business strategy can convert into customer value and market opportunity.”
– Tim Brown CEO, IDEO
