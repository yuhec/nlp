Agriculture Drone Market Set to Surge 746%
April 8, 2016 10:42 AM
Drones are ready to take off in agriculture.
© Ben Potter
Email
Think drones are already a red-hot topic in the agriculture industry today? Just wait, according to a report from RnR Market Research.
According to the report, the worldwide market for agricultural drones currently sits at $494 million, but RnR expects that amount to balloon to $3.69 billion by 2022.
Drones, sensors and other so-called “digital agriculture” tools are in line with consumer trends of demanding end-to-end transparency of how their food is produced, according to lead author of the study, Susan Eustis.
“Transparency is one of the benefits … that drones bring to digital farming,” she says. “The benefits of digital farming are higher productivity and more efficient use of land, water and fertilizer. Transparency in farming is being asked for by consumers. Consumers want to know where their food came from, how much water and chemicals were used, and when and how the food was harvested.   They want to know about consistent refrigeration during transport.”
Drones are one way to help farmers gain higher productivity and more efficiently use their land, water and fertilizer resources, Eustis says. She adds that venture investment in agricultural drones is very strong. Total ag tech capital investments doubled from 2014 to 2015, to $4.25 billion in total.
Future improvements to drones, including better materials, sensors, imaging capabilities and more, will continue to fuel growth in the ag sector, Eustis says. Drones and other ag technologies are helping farmers make better decisions in near real-time, she says.
“It is a totally different world than walking out of the farmland, kicking the dirt and making a decision based on intuition,” she says.
To learn more ways drones are being used on the farm, review potential legal issues and safety best practices, access a buyer’s checklist and more, visit www.agweb.com/drone-zone/ .
 
