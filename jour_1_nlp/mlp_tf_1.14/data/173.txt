 
Applying a Gender Lens to Agriculture: Farmers, Leaders, and Hidden Influencers in the Rural Economy
In this 14-page issue brief, the second in Root Capital’s Issue Brief Series, we share our experience of applying a gender lens to our work in smallholder agricultural finance. Through our Women in Agriculture Initiative, we have been able to better understand the areas in which we know we support women (as farmers, agro-processing employees, and leaders). This work has also identified new areas for potential impact that further foster economic empowerment for women, underscoring the vital nature of women in less conspicuous—but high-impact—roles and positions.
In this issue brief, we explain: 
An entire tier of women employees we call “hidden influencers” who often go unnoticed but are critical to making businesses function properly
How we evaluate gender-related outcomes within our portfolio using our gender scorecard to account for the range of roles, impacts and particular challenges of women
Financial products and training programs that have a disproportionately high impact on women
Our way of approaching gender lens investing, based on the nuances we see in the communities in which we work
 
© 1999 - 2016 Root Capital Privacy
 
