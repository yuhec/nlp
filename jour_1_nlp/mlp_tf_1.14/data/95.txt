Wednesday, July 22, 2015 - 2:00am
Vine Rangers has piloted its drone-driven data service with vineyards in California.
When I spoke with Vine Rangers co-founder and CEO David Baeza this spring, his business model sounded intriguing but his ability to execute was bound up in government red tape. In late April, however, the agricultural drone and analytics company got federal clearance to officially start testing its ideas.
As of July 20, the Federal Aviation Administration had greenlighted 833 proposals for using unmanned aerial vehicles, aka drones, for business purposes. More than 70 of the approved experiments are focused on applications in precision agriculture, including the projects launched by Vine Rangers .
Its mission: Use aerial images of vineyards that can be used to guide fertilizer applications, irrigation decisions, even detect disease. Vine Rangers analyzes the data and presents it to managers via a web service. It gathers information during the midday, when leaf chlorophyll is at its peak and the most data can be gathered.
“ Drones have sex appeal . They are, at their heart, little data gathering machines,” said Baeza, previously an executive with the cloud services division of software company Citrix Systems.
Vine Rangers, based in Santa Ynez Valley, California, has piloted its approach with several vineyards. Most farmers and vineyard managers have access to plenty of data, Baeza believes — they just don’t have an easy way to harvest it for meaningful insight. His startup plans on updating information on a weekly basis.
“Farmers can’t react to it more frequently than that,” he said. Although it’s still early days, Baeza was considering a price of $20 per acre for the service when I interviewed him several months ago.
Dozens of other drone companies are taking to the sky in the cause of smarter, greener agriculture. Based on my research into the project descriptions, the large majority aim to collect crop and land images that can be evaluated using third-party software for clues about stress.
Here’s my own personal watch list, chosen for purely subjective reasons. My main filter: Companies such as Vine Rangers that provide some level of software or business intelligence around the data that drones are gathering.
Here are seven, in alphabetical order:
1.  AeroHarvest — Like Vine Rangers, this California company is focused on vineyards. Its analytics services are focused on leak detection and optimizing irrigation schedules.
2.  AgWorx — A specialist in precision agriculture from Raleigh, North Carolina, that handles everything from harvest timing data to software applications for data collection on the ground and (now) in the air.
3.  Digital Harvest (formerly BOSH Precision Agriculture) — The Newport News, Virginia, company’s specialty is “agricultural informatics.”
4.  Leading Edge Technologies — Based in Winnebago, Minnesota, the company turns drone data into “farm intelligence” for applications such as grain management and other precision agriculture solutions.
5.  PrecisionHawk — This startup from Raleigh is creating an “algorithm marketplace” to help interpret and correlate data collected from satellites and drones. Potential applications include environmental monitoring. Its backers include Intel Capital.
6.  Trimble Navigation — Aside from its massive GPS business, the Sunnyvale, California, company is huge in precision agriculture with applications for everything from yield monitoring to water management.
7.  Wilbur-Ellis — The $3 billion farm equipment supplier, based in San Francisco, is working hard to bring satellite and drone imagery to AgVerdict, its software for agronomists. 
Topics: 
