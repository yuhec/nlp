FAA Publishes Proposed Drone Regulations
Posted on
Much anticipated proposed federal drone regulations were published by the Federal Aviation Administration last week.
Current Status of Federal Drone Rules in the United States
Currently, the ability to use a drone legally in the US depends on the type of use to which the drone is being put.
The use of drones for commercial purposes is banned in the United States.  Absent express permission from the FAA, drones may not be flown for commercial purposes under current rules.  The first time this permission was granted to an agricultural operation was earlier this year, when the FAA issued permission for a company to use a drone for “crop scouting.”  [Read article here .]
Using a drone for recreation or hobby, however, is allowed provided that FAA rules are followed.  [Read article here .]
Where agricultural use fits in this scheme is somewhat of a grey area.  There are certainly arguments to be made the the use of a drone in an agricultural operation is for a commercial purpose.  In light of this, until the FAA finalizes rules regarding the commercial use of drones, farmers should use drones with extreme caution due to the chance such use may be considered commercial.
To review current federal drone rules (which will remain in place until a final FAA regulation is passed), click here .
State Drone Rules
Additionally, thirteen states (including Texas ) have passed their own drone laws.  [Read articles  here  and here .]  It is, therefore, important that drone users understand the laws in their individual state to avoid potential issues.
The Proposed FAA Regulations
The complete proposed FAA rule may be reviewed here .  The following are some provisions that could impact drones used for agriculture.
* Operators must be certified, which requires meeting certain requirements (i.e. at least 17 years old, no drug convictions, no physical or mental condition impairing ability to fly the drone), passing an aeronautical test at an approved testing location, and passing a follow-up test every two years.
* Drones must be registered with the FAA.
* Operators must have visual contact with the drone using human vision not assisted by any device other than glasses or contacts.  The operator may enlist additional “visual observers” to assist with this requirement.
* Operators and visual observers may only  operate one drone at a time.
* Drones may not weigh more than 55 pounds.
* Regulations impose a flight ceiling of 500′.
* Drones must adhere to a speed limit of 100 mph.
* 3 miles minimum visibility from control station is required.
* Drones may be flown no closer than 500′ below and 2,000′ horizontal from any clouds.
* Drones may not be flown over any persons not involved with the operator and not in a covered structure that would protect them from a falling drone.
* Not surprisingly, drones may not be flown within certain airspace designated for airports.
* Night use is prohibited.
The Potential Use of Drones in Agriculture
Many have heralded drones as offering major utility for the agricultural industry.  In fact, agricultural use was the reason drones were initially developed and still has the potential to be the biggest market.  [Read article here .]  The ability to use drones in precision farming, crop health analysis, monitoring livestock, and other agricultural uses has been deemed a “game changer” by one precision agriculture expert.  [Read article here .]  Additionally, American producers point out that farmers and ranchers in numerous other counties are using drones, putting US farmers at a technological disadvantage due to lack of regulations permitting drone usage.  [Read article here .]
On the other hand, many farmers and ranchers are concerned over their privacy rights when it comes to droves flying over their property and operations.  The proposed FAA rules do not address issues related to privacy or trespass claims, which will, instead, be left up to state law.  Agricultural producers may have cause for concern here. For example, one environmental blogger has vowed to use drones over large livestock operations to document animal welfare or environmental issues.  Specifically, he intends to use his drones in states that have passed laws prohibiting the use of undercover videos on agricultural operations.  [Read article here .]  In addition to activists, there has been concern that the government–the EPA, for example–may use drones to look in on agricultural operations.  [Read article here .]
Public Comment
The proposed rule is open for public comment until April 24, 2015.  You can share your comments by clicking here .
Once comments are received, the FAA will review all comments and then make modifications (if any) to the rule.  It will likely take several months or longer for the final rule to be implemented.
This entry was posted in Drones . Bookmark the permalink .
Post navigation
