Drone, Mapping, Software, and Service
The premiere drone and software solution for agriculture, mapping, and surveying
Fixed-Wing Drone
Rugged drone featuring a full-composite exoskeleton for industry leading durability.
Two Cameras
High resolution RGB and NIR cameras. Twice the information in a single flight.
Complete System
Everything you need to begin operating including drone, case, tablet, and support.
HoneyComb Farm™
Image processing and data management. Process, annotate, export, and share.
Sign-up for a web demonstration and learn more
