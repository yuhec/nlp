Capital Press
Published on July 5, 2016 4:27PM
Sean Ellis/Capital Press A drone operator explains the potential uses of unmanned aircraft systems in agriculture after a demonstration flight over a Southwestern Idaho vineyard in April 2015. Industry expert say the FAA’s new rules for drones will open the door for the average farmer to use the technology.
Buy this photo
New federal rules for the operation of small drones will lead to more innovation in the agricultural industry and open up their use to the average farmer, industry experts say.
“I think it will be a real game-changer for extending the use of unmanned aircraft systems in precision agriculture,” said Donna Delparte, who is leading Idaho State University’s research on the use of drones in agriculture. “It’s going to really be a ground-breaking technology for growers.”
The Federal Aviation Administration on June 21 released rules for the use of unmanned aircraft systems weighing less than 55 pounds. They go into effect in mid- to late August.
Ryan Jenson, CEO of HoneyComb Corp., an Oregon drone manufacturing company, said the rules are a “big deal” because they remove the uncertainty that dogged the industry.
He said his company has already received increased interest from potential buyers and dealers and expects to double capacity in the next year.
“Now the U.S. market is really primed to take off,” he said.
Matthew Balderree, chief UAS pilot for PrecisionHawk, a North Carolina drone company, said the FAA’s requirements for drone use were in a gray area before.
“It clears up the rule aspect of it,” he said about the new regulations. “It puts it out in black and white.”
He said one of the highlights of the new rules is that drone operators no longer will be required to have a pilot’s license.
People who wanted to fly drones previously had to obtain an exemption from Section 333 of the FAA Modernization and Reform Act of 2012, the rules for certifying the airworthiness of aircraft. It was a lengthy process that required a lot of paperwork, Delparte said.
Now they must register their drone with the FAA and pass an aeronautics knowledge test to qualify to fly a drone.
“It is going to be so much easier for farmers” to use drones, Balderree said. “The barrier for entry ... is a fair amount easier.”
In an explanation of the new rules sent to Capital Press, PrecisionHawk Executive Vice President Thomas Haun said, “the new rules allow for a much broader access to drone technology that can be used by a wider audience of people.”
With the new rules, “as long as you meet a few pretty minimal requirements and operate safely, you can legally operate drones in agriculture and other commercial opportunities,” he said. “What I think you’re going to see is a movement from a few very select companies operating drones previously to a broad community of farmers having access to the technology.”
The new rules require small drones to be flown below 400 feet and within the pilot’s line of sight. Operators can fly a drone from a moving vehicle if they aren’t the ones driving and if it’s done in a sparsely populated area.
The next major step for agriculture when it comes to drone use would be to allow farmers to operate them beyond line of sight, Haun said.
“This will provide much more efficiency to farmers and help keep the cost down of the drone operations,” he said.
