Search:
Drone Usage in Agriculture Could Be a $32 Billion Market
A PricewaterhouseCoopers report pegs the agriculture drone market to be worth $32.4 billion: Here’s what you should know.
Neha Chamaria
Nov 25, 2016 at 8:09AM
Agriculture could be one of the biggest drone markets. Image source: Getty Images.
You might be familiar with the idea of drones delivering consumer packages or monitoring security areas, but did you know that drones were first used commercially in Japan for agricultural purposes? In fact, agricultural efficiency is poised to take a big leap with drone technology now that the U.S. Federal Aviation Administration is streamlining regulations for unmanned aerial vehicles. A recent report from  PwC pegs the addressable market for agricultural drones to be worth a whopping $32.4 billion, second only to infrastructure.
That's a big number. And the interesting part is that PwC isn't the only one expecting drones to revolutionize agriculture.
Bank of America Merrill Lynch projects agriculture to make up almost 80% of the commercial drone market in the future, with the potential to generate $82 billion worth of economic activity in the U.S. between 2015 and 2025.
Goldman Sachs predicts the agriculture sector to be the largest user of drones in the U.S. and the second largest in the world in the next five years.
Research company Markets and Markets estimates the agricultural drone market to grow at a compounded average rate of 30% through 2022.
Not surprisingly, drone makers like AeroVironment Inc. ( NASDAQ:AVAV ) are increasingly focusing on agriculture. The latest example is AeroVironment's Quantix drone, which it unveiled at the just-concluded Drone World Expo at San Jose, California. AeroVironment expects Quantix's features like one-touch launch, which makes it easier to map fields and gather instant analytics, to strike a chord with farm operators.
In fact, even technology companies are throwing their weight behind agricultural drones. Consider Raven Industries ( NASDAQ:RAVN ), which specializes in navigation and radar systems solutions. Raven took a big leap earlier this year when it became the exclusive distribution partner for AgEagle's agricultural unmanned aerial systems (UAS). AgEagle uses advanced technology like GoPro Inc. ( NASDAQ:GPRO ) cameras and sensors in its UAS to capture real-time data for crop surveillance.
AgEagle RX60 agricultural UAS. Image source: Raven Precision.
Considering that Raven already has long-standing partnerships with leading agricultural-equipment manufacturers like Deere & Company ( NYSE:DE ) and AGCO Corporation ( NYSE:AGCO ), I wouldn't be surprised to see more and more agricultural companies pump money into drone technology in coming years.
Wait, they're already doing it!
Agricultural companies are getting serious about drones
This past April, DuPont ( NYSE:DD ) invested an undisclosed sum in drones company PrecisionHawk. The news took the market by surprise, as it isn't every day you see an agricultural company turn into a venture capitalist. Clearly, DuPont must be expecting the agricultural drone market to take off to have made the move. PrecisionHawk cited how both companies "recognize the opportunity for drone technology solutions in agriculture," backed by analysts' estimates that peg the "economic impact" of the technology to cross a whopping $60 billion in 10 years.
On the other side of the agriculture spectrum, farm-equipment manufacturers are quietly digitalizing farms by adopting precision agriculture -- a modern farm management concept that uses sensors to scan, record, and collect data from fields. Over time, the scope of precision agriculture has expanded vastly to include GPS, automated systems, geomapping, and satellite imagery. Drones are the latest addition to the list.
Several drone technology and software companies like Sentera and Agribotix have recently tied up with Deere Operations Center to extend their products and solutions to Deere dealers and customers. The Deere Operations Centre platform allows users to gather, analyze, and manage data related to equipment and farm operations virtually. You may call it remote farming. In line with its focus to automate agriculture, Deere is upgrading its Operations Centre with new mobile apps and software.
But none of it comes even close to what AGCO has done on the drone front. Hold your breath: AGCO already has a UAV to its credit! Launched last year, the SOLO AGCO Edition drone is equipped with GoPro cameras and can scout about 240 acres in 20 minutes to provide high-resolution aerial field maps.
SOLO AGCO Edition UAV. Image source: AGCO.
With AGCO already taking the plunge, you may soon see more and more drones flying over farms, especially given how beneficial drones can be to farmers.
How drones fit into agriculture
Farmers today face one of the world's biggest challenges: to feed a growing population amid weather blips and shrinking arable land. The key lies in boosting crop yields -- something drones can help achieve.
Agricultural drones are high-tech systems that can do things a farmer can't: scan every corner of the fields to assess soil, monitor crop health, apply fertilizers, even track weather and estimate yields, and then collect the data and analyze it for prompt action. In short, drones can mechanize every step of farming, eliminating the costs of human errors and enabling farmers to react quickly to threats (such as drought conditions and pests), helping them maximize income and returns on investment in the end.
Long story short, drones can transform agriculture, and with the market potential now pegged at a whopping $32 billion, investors should keep a close watch on the companies that are investing in the technology.
Neha Chamaria has no position in any stocks mentioned. The Motley Fool owns shares of and recommends GoPro. The Motley Fool is short Deere & Company and has the following options: short January 2019 $12 calls on GoPro and long January 2019 $12 puts on GoPro. The Motley Fool recommends AeroVironment and Raven Industries. Try any of our Foolish newsletter services free for 30 days.
We Fools may not all hold the same opinions, but we all believe that considering a diverse range of insights makes us better investors. The Motley Fool has a disclosure policy .
Author
( Nehams )
A Fool since 2011, Neha has a keen interest in materials, industrials, and mining sectors. Her favorite pastime: Digging into 10Qs and 10Ks to pull out important information about a company and its operations that an investor may otherwise not know. Other days, you may find her decoding the big moves in stocks that catch her eye. Check back at Fool.com for her articles, or follow her on Twitter. Follow @nehamschamaria
Article Info
Nov 25, 2016 at 8:09AM
Energy, Materials, and Utilities
