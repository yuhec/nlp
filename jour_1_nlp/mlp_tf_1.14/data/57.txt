TrialDrone?
Social Media Intelligence for Trial Lawyers
TrialDrone is an exclusive technology that is revolutionizing the way Trial Lawyers prepare cases for litigation. We provide comprehensive social media intelligence gathering that searches and analyses billions of open source public social media posts.
Tactical access to social media insight for trial
Looking for the edge at trial?
Watch this TrialDrone Video to learn what our technology can do for your case
TrialDrone’s social media search capabilities offer a number of significant tools for lawyers preparing for trial, including:
The ability to reach back into time and re-create an event, capturing the identity of potential witnesses at the very scene of the event at the very time that it happened and discovering potentially relevant evidence that would otherwise be lost – adding significant value in terms of costs and more importantly, results at trial.
Being able to monitor a witness in real time and receive immediate alerts of their online statements, locations and activity.
Being able to create a library of an adverse witness or party’s prior online statements – capturing not only the prior statements but also the specific location that the statement was made (where the geo locate function on their phone is enabled)
The ability to create a map of a target witness or party’s social media contacts – potentially identifying connections that may have been undisclosed
This technology is currently being used by intelligence agencies and police forces around the world to identify risks to public safety.  It is now available for lawyers – providing game changing intelligence at a fraction of the cost of traditional investigators, and providing it fast – getting potentially critical intelligence reports into your inbox within 48 hours, and sometimes sooner.
