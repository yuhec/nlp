Learn More
Drones: A Game Changer For Agriculture
Agricultural drones represent a new way to collect field-level data. The most compelling reason for using drones is that the results are on-demand; whenever and wherever needed, the drone can be easily and quickly deployed. A grower or service provider can have a drone in the back of the truck and get actionable, field-level information the next day or sooner. It’s hard to beat the immediacy and convenience of planning the mission, collecting the data, and getting near real-time results; only drones offer these benefits.
Drones are affordable, requiring a very modest capital investment when compared to most farm equipment. They can pay for themselves and start saving money within a single growing season. Operation is relatively simple, and getting easier with every new generation of flight hardware. They’re safe and reliable. They are easy to integrate into the regular crop-scouting workflow; while visiting a field to check for pests or other ground issues, the drone can be deployed to collect aerial data. Yet, the real advantages of drones are not about the hardware; the value is in the convenience, quality and utility of the final data product.
Your time is valuable.  The Agribotix systems are designed to let you focus on identifying problems, not learning how to operate a new software package.  At the end of a day of flying, simply put the data chip into your computer an our custom software will take it from there.
Its just a simple process:
Survey Your Field
Planning a survey is as simple as selecting the edges of the field, defining flight parameters and uploading it to the drone via a wireless link.  The drone then flies the plan automatically and returns to the starting location to land.
Upload To Farm Lens
At the end of a day of flying, remove the data chip from the drone and with a couple of clicks on the included Windows software send the data to FarmLens for processing.
Analyze Your Data
Within a couple of business hours receive an email letting you know your results are available on FarmLens for viewing or downloading.
Compare
No matter what you are looking for, we have a drone for you.  New or infrequent user, the Agrion is the perfect system; easy to fly, easy to carry, and easy on the wallet.  For heavy users, the Enduro will meet your needs.  In specific situations, the Hornet fixed wing might be appropriate.
Features
