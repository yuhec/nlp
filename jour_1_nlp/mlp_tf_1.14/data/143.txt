Dog and Cat Relationships
Making Them Work
Piper and TC are VPI employee pets who live together

We’ve all heard the phrase “fighting like cats and dogs,” mainly to describe two animals (or humans, for that matter) that are always at odds with each other. However, relationships between cats and dogs are possible, depending on their personalities and their owners’ patience and understanding. And while the initial introductory period can be tricky, this unique relationship can be quite rewarding both species.
Dog and Cat Behavior

By nature, dogs are predators and often chase things smaller than them—including cats. However, this doesn’t mean that dogs and cats are not able to live in harmony. As the two most common household pets, the way dogs and cats relate to each other have a lot to do with their temperament, and whether either have had any adverse reactions to members of different species in the past.

For example, a dog raising his paw to a cat may mean he wants to play, but a cat can take it as a sign of an attack, prompting the cat to swat her paw at the dog, sending her into a hissing fit. Likewise, a cat that tries to rub up against a dog may be acting friendly, but a dog can interpret that as a threat—especially if the cat is near his toys or food—and can cause the dog to growl or bark.

However, getting a puppy and a kitten at the same time and raising them together is an option: that way neither has previous fears of each other, unless one of the animals is overly-aggressive and domineering.
Coexisting Peacefully

It is possible for cats and dogs to coexist together and build a friendship, says Bobbie Cooper, director of partnership marketing for Veterinary Pet Insurance (VPI).
Katy and Flip are VPI Pet Insurance employee pets who live together

“When we first got our dog, Pearl, our cats hissed and put up a fight; as a small puppy, Pearl was somewhat intimidated by them,” she says. “But as time passed, they became friends, and before long, the dog and our two cats were curled up sleeping together on the couch.”

Cooper says that Pearl, her six-year-old Schnoodle (a cross between a schnauzer and a poodle) even cleans Cleo, her 12-year-old tabby’s, ears. “You see Cleo sitting there with her eyes closed as Pearl licks her ears, and you can just tell she is in heaven.”
