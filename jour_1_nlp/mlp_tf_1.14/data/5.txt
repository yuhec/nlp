Drones create new opportunities, challenges for agriculture
by  Neil Lyon, 23 March 2017
Share on Facebook Share on Twitter Share on Google Plus Share on LinkedIn Share on Pinterest Share via Email
AS DRONES open up a new world of opportunity for Australian agriculture, farmers are grappling with how to most effectively utilise the technology and deal with emerging issues of data ownership, regulation, liability and privacy.
Technology enthusiast and drones guru, Dr Catherine Ball, Brisbane, told growers at a Conservation Farmers field day near Jimbour on Queensland’s Darling Downs that drone technology was moving so fast it was leaving many people behind.
“Drones are just another sensor data collection mechanism. My biggest piece of advice is don’t care about the drones, care about the data. Ask the reason why you are collecting it and that will probably lead you to the better business solutions you might want,” she said.
“When you start looking at the level of data collection that is done so easily and quickly using drones, you have to ask yourself are you able to get the information from it you need? Pretty pictures are great, but do they mean anything to you?
“While you are collecting the data, is it actually going to be a help or hindrance?”
Dr Ball said a wide-ranging discussion was needed around drones because they were being flown and operated in ways that differed from traditional aviation. That raised a whole series of new questions.
One of the key concerns is that of privacy and the rights of landholders in the agricultural sector.
Dr Catherine Ball
“Currently in Australia I could fly a drone over your land right now and would not need your permission,” she said.
“If I am flying at 120 metres above your farm with high resolution, sub-military-grade photographic equipment you won’t even know I’m there.”
Dr Ball said that raised the question of who owned the data collected from drone overflights of farm land.
“I could fly over the top of your farm and do whatever I want to with the data I collect,” she said.
“I don’t think that is right because what I could also do with that data is publish it online without your permission.
“I could actually take your yield data, plant health data, biosecurity data and sell it to insurance companies.
“So, we have to start looking at what kind of data is collected, who you want collecting your data and what you want to be done with it.”
Despite the challenges, Dr Ball said there was tremendous scope for using drones in a whole host of beneficial applications that were yet to be fully realised. New applications for the technology were being found every day.
The applications for using drone technology in agriculture are limitless. (Photo: NAB)
“Up north the Department of Primary Industries is now funding citizen science projects where people submit their drone photography on their holidays and the photos automatically go into a filter looking for weed species. They are actually doing biosecurity measurements on people’s holiday snaps from their drones,” she said.
“We were able to fly drones over banana crops last year when there was the Panama Race 4 virus outbreak. So, instead of walking farm to farm and risking taking soil-borne pathogens between farms, you can just fly over the top. You can see that data using very basic cameras and very basic filtering.
“And one of the things I have been watching for a while is the ability to use drones to automatically seek and destroy pests and weeds. From a biosecurity and crop management perspective you can precision map and then precision spray which means you use a lot less herbicide. You can go from massive field level spraying to precision level spraying.”
Grain Central: Get our free daily cropping news straight to your inbox –  Click here
Dr Ball said when it came to regulating drones in agriculture, the Civil Aviation Safety Authority (CASA) had been very open with landowners and had allowed many exemptions.
“You can fly aircraft up to 25 kilograms in weight on your land without needing certain permissions that other people would need to have if they were bringing in a drone and flying it on your land,” she said.
“They are trying to open the doors and make it easier for landowners to use drones for commercial purposes without being beaten up with all the regulation.”
……………………………………………………………..
What are the basic rules for operating drones?
AUSTRALIA’s safety laws for drones or remotely piloted aircraft (RPA) generally depend on whether the operator is flying commercially or recreationally.
If you are flying for any economic gain you need certification unless your RPA weighs less than two kilograms. Under these circumstances, you need to notify CASA and follow the standard operating conditions (SOCs). There are also reduced regulatory requirements for some private landowners/leaseholders operating RPAs.
If you are flying for fun and not any economic gain, then the regulations are less restrictive. You do not need to be certified, providing you follow some simple safety rules.
Whatever your reason for flying, it is an offence to operate an unmanned aircraft in a way that creates a hazard to another aircraft, another person or property.
Very small RPA commercial operators
Commercial operators flying very small (< 2 kg) RPAs do not require an RPA operator’s certificate (ReOC) or a remote pilot licence (RePL).
You are required to notify CASA at least five days before your first commercial flight and operate by the SOCs. This means obtaining an aviation reference number (ARN) from CASA.
Flying over your own land
Private landowners and leaseholders can carry out some operations on their own land with a small RPA (< 25 kg) without needing a ReOC or RePL, if you follow the SOCs and none of the parties involved receives remuneration. Activities include aerial spotting, photography, spraying and carrying cargo.
Under these conditions, you can also operate a medium (25-150kg) RPA providing the remote pilot holds an RePL.
What are the standard operating conditions?
Excluded RPAs (i.e. commercial very small RPA operators and some private landowners) must follow the standard operating conditions.
You must only fly during the day, not at night.
You must only fly by visual line of sight (VLOS)-close enough to see, maintain orientation and achieve accurate flight and tracking.
You must fly no higher than 120 metres (400 feet) above ground level.
You must not fly any closer than 30 metres from other people.
You must not fly in a prohibited area or in a restricted area without the permission of the responsible authority.
You must not fly over populous areas, such as beaches, parks and sporting ovals. The risk to life, safety and property depends not only on the density of people and property in an area but also the flying height and the likelihood of injury or damage should something go wrong with the RPA.
You must not fly within 5.5 kilometres (3 nautical miles) of a controlled aerodrome (one with an operating control tower).
You must not fly in the area of a public safety operation without the approval of a person in charge of the emergency response. This includes situations such as a car crash or any police, firefighting or search and rescue operations.
You must only fly one RPA at a time.
Excerpts from CASA rules for flying drones: https://www.casa.gov.au/standard-page/flying-drones-australia
