Senate appropriations proposal may jump-start MPP-Dairy changes
Much of the agricultural community’s conversation surrounding the use of “drones” has concerned the potential positive aspects of inspecting cropland or viewing grazing cattle in remote areas. However, unmanned aerial vehicle (UAV) technology creates the potential for animal rights and environmental activists to spy on concentrated feeding operations (CAFOs) and other farming practices.
According to reports circulating on social media, Will Potter, an independent journalist, raised about $75,000 through an online Kickstarter campaign to conduct drone surveillance in his opposition to so-called “ag gag” laws. Potter recently appeared in a segment of Truth and Power, a 10-part series on PIVOT television, in defense of environmental and animal rights activities.
While debate and legal haggling continues over ag gag laws, activists’ use of drones to spy on farming operations is also an area of “unsettled law,” according to attorneys specializing in agriculture who were contacted by Progressive Dairyman.
Tiffany Dowell Lashmet, assistant professor and Extension specialist in agricultural law with the Texas A&M AgriLife Extension Service, has written extensively on drones and potential farm privacy issues.
“As often happens, the law has fallen behind the technology, leading many people to question, or incorrectly assume they understand, how private property rights and the use of commercial drones will coexist,” she said.
Trespass and nuisance
Now that the skies are a public highway, there is no longer a “magic” height to which the owner’s property line extends. Gone are the days when a property’s boundary reached the heavens, adding questions as to where and when trespassing occurs.
“A drone flying over one’s property may not, in fact, be trespassing,” Dowell Lashmet said. “We don’t know where the line is at this point, and until the court rules on a case, we may not have a black-and-white answer.”
A second common claim is that a drone could be considered a nuisance. However, if a person claims nuisance based on noise made by a drone, for example, Dowell Lashmet said the noise would have to be enough to substantially impair the use of property by an ordinary person.
Don’t shoot them down
One thing is clear: If you suspect an activist is spying on your farming operation, don’t blow the drone out of the sky.
“You should not shoot down a drone since it is considered an aircraft in the eyes of the Federal Aviation Administration (FAA),” said Todd J. Janzen, Janzen Agricultural Law LLC, Indianapolis, Indiana.
“Shooting down a drone could be seen as akin to shooting down an airplane, and could result in serious consequences, including terrorism charges,” Dowell Lashmet said. “Another potential claim that could be brought by a drone owner is destruction of private property.”
Criminal charges have already been filed in drone shooting incidents in several states.
What are the options?
If you can’t knock a drone out of the sky, what recourse is there?
While cloudy, legal claims of trespass and nuisance still potentially apply. States have started to adopt statutes to govern these issues, and about 20 states have drone privacy laws on the books, according to Dowell Lashmet.
Some drone-related statutes specifically address capture and use of images with the intent to conduct surveillance.
The Texas law, for example, does not allow drones to capture, possess, disclose, display or distribute an image of an individual or private property with the intent to conduct surveillance. While a violation of these provisions is a misdemeanor, the individual or property owner may bring a civil suit against the person operating the drone, with penalties of $5,000 to $10,000, plus actual damages.
Images captured illegally may not be used as evidence in any legal proceedings.
As unsettled as the law is regarding drones, it’s best to leave any drone wars to the courtroom, Dowell Lashmet concluded. PD
Read Dowell Lashmet’s part one and part two blog posts.
PHOTO: Shooting drones that fly over your property could potentially be seen as shooting an airplane in the eyes of the law and may lead to terrorism charges. Photo by Kevin Brown
Dave Natzke
