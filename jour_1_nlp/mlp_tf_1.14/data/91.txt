Home John Fischbeck 2017-07-14T11:42:19+00:00
Welcome to
FAA Certified Flight Instructors and Part 107 Training Preparation
 150+ years of Aviation and UAS Experience
We Are Passionate About Providing You With The Best Professional Drone Pilot Preparation
Be at the cutting edge of technology and innovation by keeping up with the industry of tomorrow. It is estimated that the drone industry will create 100,000 jobs and contribute US $82 billion to the US economy in the next decade; 70% will occur in the first three years. Now, with the recent FAA decision to began approving commercial drone applications they have created immediate employment opportunities. Learn the skills today that you will need to profit in this emerging commercial market.
The Drone Universities program provides a comprehensive training designed to help you pass the part 107 Airman’s knowledge exam, which is an important part of becoming a Pilot in Command or PIC. At Drone Universities, we help you prepare for today for the safe operation of UAS in the National Airspace System. We also provide our students with free refresher courses to help keep your skills and knowledge current.
Our comprehensive program teaches you everything to know to get started in the drone industry. With an emphasis on hands-on practical training provided by some of the industry’s best pilots, our curriculum is designed to fast track our students into working drone professionals. We teach our students about different flight systems, various types of UAV’s used in the industry, regulations, safety, 107 test preparation and what it takes to be a professional drone pilot. Our goal is to help you master the basics, understand what to expect on the job, ask questions and interact with working professionals while learning safety procedures and most of all, eventually build your own drone business.
Beginning and advanced classes are available. Course registration is now open. Enroll today and receive a free DJI Phantom.
Find a class near you and register today .
Student Testimonials
I am so glad that I took the course with Drone Universities. It opened my eyes to so many potential things that I had never even thought of. I had so many questions before taking the course and would spend countless hours online trying to find the answers but there was so much misinformation out there. Drone Universities answered all of my questions in an informative and comprehensive manner. The instructors were very knowledgeable and personable. I enjoyed the classroom instruction as well as the hands on experience. When I first started the course I could barely fly a drone. By the end of the second day I was confident enough to take my new drone home and fly solo. I think that anyone interested in flying drones, even as a hobbyist, should take this course to learn the safe practices and regulations. Safety is the key to success!
Melissa M, Todd Stanley Productions and Ridgeline Entertainment
I received a DJI Phantom 3 this past Christmas and as a new drone owner, I had no idea where to start. I knew I needed to register my drone, but that’s about it. That’s why I enrolled in Drone Universities drone safety course. Not only did I learn how to fly my drone safely, I now understand basic drone law. Thanks to Drone Universities, I’m now registered/compliant with the FAA and I’m confident when I fly my DJI Phantom.
I’d had a great experience and strongly recommend Drone Universities.
Isaac E, New Drone Owner/Drone Hobbyist
I had the opportunity to attend the 2-day training of Introduction to Drones at Drone Universities, after attending a single day training from another drone school. I can vouch for the good quality of training provided by their team. The training was highly ethical and the trainers were flexible with our needs. I recommend them for 3 reasons:
They focus on making you a good pilot with deeper theoretical and practical knowledge
They are real pilots. Each instructor taught course content with insights from personal experiences
They teach with passion and want you to be successful
I plan to attend their advanced level courses in the near future.  They may not be cheapest but the quality speaks for itself. I admire their passion and sincerely wish them success.
Omair H, Fortune 100 IT Solutions Architect
Having taken Drone Universities course Intro to Drones, I realize it was the best investment I made. Not only do you learn the basics of this industry, you are also given a chance to learn to fly them! Not many programs out there that I have taken give you this option; I’ve tried others. The staff was extremely knowledgeable and friendly. I strongly recommend them!
Jess G, Aspiring Drone Operator
I recently attended “Introduction to drones” offered by Drone Universities. The class was taught by several instructors who were experts in their domains. Their knowledge, experience and expertise spoke for itself. The focus of the class was on how to be a good pilot by having the best ethics, related knowledge and professionalism. One thing that impressed me the most was the emphasis on actually learning how to fly a drone without, and without always needing technology as an aid.
The class overall was an excellent experience and I am planning to attend more classes from Drone Universities.
Faisal O, Software Engineer/Drone Hobbyist
The GIS application was very amazing. I was able to identify additional opportunities for providing solutions to clients in mining and cement manufacturing. … The cinematography course was superb.
Mamoud A, Drone Hobbyist
Very good and informative class. Glad I signed up for it. … Instructors were terrific. So much info to learn in one day.
Tyler H, Drone Enthusiast
As a professional in my field, it is easy to recognize expertise in others. We had access to 3 experts in this class that added a depth of experience and background that made this training invaluable.
Russell H, President, Public Sector Partners
Certainly an excellent forum for evaluating the emerging drone industry and how to proceed from a commercial viewpoint, along with making a strategic alliance.
Rob B, Geografx Digital Mapping Services Canada
The Team at Drone Universities are very knowledgeable in all aspects regarding industry regulations as well as professional applications. A great course to gain further insight and practical operations. Thanks!
Ken K, Drone Enthusiast
Instructors are great and knowledgeable, and have real interest in teaching to fly safely and improve the drone industry.
Alberto O, M.D. Adjunct Professor
The teachers were very good with explaining the topics and answering my questions! I would suggest this course to anyone that is interested in this field.
Marshall M , FAA Part 107 Commercial Drone Pilot
Why are We Different?
