February 17, 2016 /  9:40 PM / a year ago
Ag tech investments double in 2015 despite U.S. farm slump: study
Karl Plume
3 Min Read
A U.S. and Iowa state flags are seen next to a corn field in Grand Mound, Iowa, United States, August 16, 2015.Jim Young
CHICAGO (Reuters) - Investments in agriculture technology startups surged to a record $4.6 billion in 2015 despite a steep drop in U.S. farm income and slumping profit at farm-affiliated companies such as machinery producers and seed makers, according to a study released on Wednesday.
The investments were nearly double the $2.36 billion seeded by venture capitalists and others in 2014, according to the annual report from online food and agriculture investment platform AgFunder.
The jump came as net U.S. farm income is projected to drop for a third straight year and as industry stalwarts such as equipment maker Deere & Co and seed and agrochemicals company Monsanto Co face slumping sales and job cuts.
"There's definitely been a downturn in the broader market, but ultimately the direction of agriculture is going toward an more technologically driven future," said AgFunder CEO Rob Leclerc.
Investments in 2015 were also noticeably less American, as just 58 percent of deals were in U.S.-based companies, versus 90 percent in 2014.
Food e-commerce companies, including online grocers and meal delivery services, raised $1.65 billion in 2015, led by hefty funding rounds by COFCO [CNCOF.UL] subsidiary Womai, U.S.-based Blue Apron and Germany's HelloFresh.
The sector was by far the year's most active with 36 percent of total investment, leapfrogging 2014's top segment, bioenergy, which faced headwinds from weakening oil prices. The e-commerce segment's growth, however, could be limited by market saturation, Leclerc said.
Drones and robotics investments grew by 237 percent from 2014 and AgFunder pointed to a bright future for the segment as more growers are expected to use crop-scouting drones and various autonomous farm machines for data analysis and precision farming.
Satellite imagery company Planet Labs and China-based drone maker DJI raised the most within the precision agriculture sector. Also, 19 different precision agriculture software companies raised capital in 2015 as more farmers are expected to tap data science to boost yields and lower costs.
"Ag tech startups are capitalizing on the consolidation and professionalisation of farming. Lower commodity prices will accelerate both trends," said Sid Gorham, CEO of farm management software company Granular, which raised $21 million last year.
Concerns about climate change, sustainable farming and soil health propelled investment in soil and crop technology companies, which raised $168 million. Biotechnology company AgBiome scored the sector's largest deal with investment from the Bill & Melinda Gates Foundation, the venture capital arms of Monsanto and Syngenta and Pontifax AgTech.
Editing by Matthew Lewis
