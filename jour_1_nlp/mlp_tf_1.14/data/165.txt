Dealing in Drones: The Big Business of Unmanned Flight
Brad Quick
Friday,  3 May 2013 |  2:10  PM ET
CNBC.com
Tom Williams | CQ Roll Call | Getty Images
Sen. Patrick Leahy, D-Vt., holds a Draganflyer X6 drone at a Judiciary Committee hearing.
"Drone" is a loaded word. Just saying it automatically triggers images of the military's Predator program, which gained notoriety in the skies over Iraq, Afghanistan and Pakistan.
It's an image the Unmanned Aerial Vehicle industry is hoping to shed.
Congress has set Sept. 30, 2015, as the Federal Aviation Administration's target date to safely integrate UAVs into the national airspace.
The agency estimates as many as 7,500 drones could be in commercial use over U.S. towns and cities within five years.
And while civil liberties groups like the ACLU cry foul over what they see as an increase in domestic surveillance, the UAV industry says the economic benefits of drone integration are being overlooked. The industry claims it will create 100,000 jobs and contribute $82 billion toward the GDP by 2025—but only if the FAA hits its 2015 target date.
show chapters
Friday,  3 May 2013 | 12:00  PM ET
So the agency finds itself toeing a fine line between innovation and public outrage.
"We recognize that the increasing use of unmanned aircraft does provide privacy concerns, and those concerns need to be addressed," an FAA representative said. "But we've been integrating new technology into the airspace for more than 50 years, and we expect to be able to do the same thing with unmanned aircraft."
(Watch: Miami Police Use Hi-Tech Drones to Patrol Skies )
But with many state and local governments implementing their own restrictions on drone flight, hitting that 2015 target date could become more complicated. Currently 34 states are trying to pass legislation that restricts the use of UAVs.
In February, Charlottesville, Va., became one of the first cities to pass a resolution banning drones for surveillance. The industry fears that this new wave of proposed legislation could hamper drone development.
(Read More: Domestic Drones Are Already Reshaping US Crime-Fighting )
According to the FAA, "Local governments could restrict the use of an unmanned aircraft by regulating their use in the agencies and universities they control." So even if the FAA says UAVs are allowed to fly in a particular airspace, the towns below that airspace could ban people, universities—even police and fire departments—from using drones within city limits.
This further complicates the FAA's plans to establish six federally designated drone testing sites around the country.
The agency hopes to use the data it collects from these sites to figure out the safest way to integrate drones into U.S. airspace. Government agencies and universities have until Monday to submit their proposals. The FAA has already received 50 applications from 37 states, but applications from organizations in states with proposed anti-drone legislation won't be viewed as favorably.
A New Growth Industry
"Our systems carry cameras on them, not weapons," said Steven Gitlin, vice president of Investor Relations with AeroVironment . Dozens of workers stream past, signaling shift change on the Southern California company's manufacturing floor.
Over the past decade, AeroVironment has seen its revenue grow by tenfold and its workforce more than double. The company, which supplies reconnaissance drones to the U.S. military and more than two dozen foreign governments, thinks it can expand even faster once drones are approved for broad commercial use in U.S airspace.
(Read More: Domestic Drones Stir Imaginations, and Concerns )
It hopes its experience building UAVs for search and rescue missions will help it target local police and fire departments looking for cheaper alternatives to helicopters. "If you look at those 40 or so thousand agencies, the vast majority of them—over 95 percent—don't have any kind of aircraft. And they all value that aerial vantage point that an aircraft can deliver."
Gitlin said that kind of growth leads to good paying jobs, "From manufacturing, engineering, technical people ... it's a real wide range of job opportunities that this industry is going to have to fill as the market starts to expand and as demand grows."
But the industry sees its biggest opportunity in agriculture, estimating as much as 80 percent of its growth will come from what it calls precision farming—drones doing everything from crop dusting to field monitoring. That's just one of the markets former WIRED editor Chris Anderson is hoping to capture.
Anderson partnered with Jordi Muñoz—a UAV enthusiast he met over the Internet—to form 3D Robotics. The San Diego-based company specializes in do-it-yourself drones aimed at hobbyists.
Anderson thinks his low-cost drones—many of which sell for less than $1,000— could find a niche with any industry looking for a low-cost, innovative edge. "It's starting to be used for commercial use," Anderson said. "A lot of this is experimental: agriculture, Hollywood, search and rescue, crop surveys, scientific sensing, things like that."
And Anderson believes it's these types of innovators that will ultimately determine the way we will view drones. "We put advanced technology in the hands of regular people, and they figure out what it's for."
Playing
