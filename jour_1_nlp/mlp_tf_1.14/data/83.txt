Drone tool for farmers wins top prize at Clean Energy Trust Challenge
Kristen Norman / Blue Sky
Todd Golly, left, Orlando Saez, center, and Kunal Bhatt, right, of Aker, react after claiming the top prize at the Clean Energy Trust Challenge.
Todd Golly, left, Orlando Saez, center, and Kunal Bhatt, right, of Aker, react after claiming the top prize at the Clean Energy Trust Challenge.
(Kristen Norman / Blue Sky)
Robert HollyBlue Sky Innovation
Aker, a Chicago-based agriculture company that uses drones to make farms more efficient, earned the biggest prize at the Clean Energy Trust Challenge pitch competition Tuesday.
Unlike similar platforms that just collect data, Aker combines aerial images with analysis from agronomists and farm retailers to help growers use chemicals more efficiently, as well as find spots to improve drainage and identify crop-smothering weeds.
Aker's prize: $400,000 in funding from the Illinois Department of Commerce and Economic Opportunity. Overall, $950,000 in funding was awarded to five early-stage clean-tech startups as part of the seventh-annual pitch competition. 
Erik Birkerts, CEO of the Clean Energy Trust, said the yearly pitch competition started as a way to connect clean-tech stakeholders from throughout the Midwest, but it's turned into an investment-focused event to support “exciting” projects that might otherwise struggle to raise capital.
More than $4.6 million in funding has been awarded to 38 startups since the competition began in 2010.
“For a lot of these science-based companies, the road they have to follow to develop and prove their technologies is so much longer than what you might see with an internet-based startup or software company,” Birkerts said. “You’re not seeing as much traditional venture capital active in this space.”
Orlando Saez, co-founder and CEO of Aker, said his company will use a portion of its prize to commercialize a field-level tool designed to gather more data from under the canopy of plants. Aker will also use the funding to bolster its sales team and expand in Illinois, he said.
“This money is definitely going to help grow our company and make farming more efficient,” Saez said. “The more farmers know, the smaller the chemical footprint they’ll have, and that’s very important for everybody.”
PowerTech Water — a Kentucky-based startup that developed technology to remove salts, corrosive minerals and toxic metals from water without the need of expensive pipe coatings or high-pressure pumps — received $200,000 from Clean Energy Trust’s general prize fund.
IdleSmart — a Kansas-based transportation startup that created an automated engine start and stop system to reduce the amount of time drivers have to idle their vehicles overnight — won a $130,000 award from Wells Fargo and a $20,000 award from United Airlines.
Sun Buckets — which developed portable solar-powered cooking stoves — won four different awards totaling $100,000. The Champaign-based startup won the $20,000 Power Clean Cities Award, the $20,000 Hanley Foundation Award and a $20,000 award from ComEd. It also received $40,000 in funding from the Clean Energy Trust prize fund.
With a second $100,000 prize from the Department of Commerce and Economic Opportunity, Lotic Labs was the only other Chicago-based startup to secure funding. Lotic Labs provides financial products and risk-management services for water-dependent businesses.
Clean Energy Trust has made equity investments in nearly two dozen clean-tech startups, including last year’s pitch competition winners NovoMoto and Nexmatix. Combined, its startup portfolio has raised more than $112 million in follow-on investment and created more than 300 jobs. The majority of Clean Energy Trust startups were founded by women and minorities, Birkerts said.
About 250 students, entrepreneurs, business executives and investors attended this year’s Clean Energy Trust Challenge, according to organizers.
Robert Holly is a freelance writer.
