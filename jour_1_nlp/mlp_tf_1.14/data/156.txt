Home Type VTOL Challenger Aerospace Group Manta Ray
Challenger Aerospace Group Manta Ray
By
-
21 January 2017
“The Manta Ray” Is a Hybrid powered Tri-copter/glider/ UAV/Drone:  The Manta Ray, like its  namesake, can move swiftly from vertical takeoff , hover to smooth transition into forward flight, the Manta Rays unique design allows  its wing morphing into a powered glider type UAV/drone giving the aircraft greater flexibility and distances.
From a stand still take off to high-speed flight, the Manta Ray combines the perks of a Tri copter’s manoeuvrability with the flight distance and speed of a fixed wing UAS.
The Challenger Airspace Group is a leading developer in the rapidly changing field of unmanned vehicles. We design subsystems, develop platforms, and manufacture airframes. We believe that by investing in every step of the process, from conception to completion, we produce a product that is unmatched in quality and unparalleled in technological advancement. We create nearly every component in house to ensure the best results.
The unmanned vehicle market is witnessing astonishing growth. Maybe the most widespread use of drones today is assisting delivery services, but this is just the tip of the iceberg.
