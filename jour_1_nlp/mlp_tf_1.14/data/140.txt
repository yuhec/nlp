Tomorrow Today
Drones in agriculture
Researchers are testing drones to see if they can help farmers fight weeds and pests. The drones can automatically map large agricultural fields, identify plants, and enable herbicides to be sprayed with precision, sparing the crops.
Watch video 05:52
