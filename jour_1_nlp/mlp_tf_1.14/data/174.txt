Drones may provide big lift to agriculture when FAA allows their use
Discover Sonoma County Wine
A drone lifts off at Kunde Family Vineyards near Santa Rosa, Calif. Ryan Kunde, a winemaker at DRNK Wines, flies his drones recreationally and has been testing drones with the goal of one day using them to help make decisions in the vineyard.
A drone lifts off at Kunde Family Vineyards near Santa Rosa, Calif. Ryan Kunde, a winemaker at DRNK Wines, flies his drones recreationally and has been testing drones with the goal of one day using them to help make decisions in the vineyard. (Discover Sonoma County Wine)
Chad Garland
A boom in drones may come from precision agriculture, using high-tech systems to help farmers increase yields
When Steve Morris began building unmanned aerial systems in the late 1990s, he envisioned flying them over fields and collecting data that would be useful to farmers.
But after the wars in Afghanistan and Iraq, drones became largely associated with military strikes and surveillance operations. Morris said the technology became the subject of contentious political debates and public paranoia.
"The entire dream evaporated at that point," said Morris, founder and president of MLB Co. in Santa Clara, Calif. "In an alternate universe where [drones] rose to prominence through helping the economy, creating businesses and jobs, people would have a different view of them."
More than a decade later, attention is refocusing on development of drones for commercial purposes. Amazon.com Inc. , Google Inc. and Walt Disney Co. are grabbing headlines with plans to develop drones for deliveries, mapping and entertainment.
But the big boom in unmanned aircraft may come from what's known as precision agriculture — using high-tech systems to help farmers increase yields and cut costs.
In recent years, consumer-quality drones that are cheaper and easier to fly have become commonplace, but Federal Aviation Administration rules have restricted their civilian use to recreation and research in all but a few cases. That has led Morris and others to market their agricultural drones overseas, where regulations are not as strict.
I think it's going to change agriculture as we know it in North America. It's definitely going to allow producers to become much more efficient. — Scott Shearer, a professor at Ohio State University and an expert in precision agriculture
Sunnyvale, Calif., technology company Trimble began offering agricultural drones in January and is currently selling them in foreign markets. Indiana-based drone maker PrecisionHawk says it has projects in Canada, South America and Australia.
California farmers and technologists from the Russian River Valley to Silicon Valley say they are eager to put drones to commercial use here at home.
Some, like YangQuan Chen, an engineering professor at UC Merced, envision a new "data drone valley" in the state's Central Valley, not far from the tech giants and venture capitalists of the Bay Area.
Discover Sonoma County Wine
Ryan Kunde, a winemaker at DRNK Wines in Sebastopol, Calif., prepares to launch a 3D Robotics Aero drone at Kunde Family Vineyards near Santa Rosa, Calif. Kunde flies his drones recreationally, but he hopes one day to use them to help farmers manage their crops.
Ryan Kunde, a winemaker at DRNK Wines in Sebastopol, Calif., prepares to launch a 3D Robotics Aero drone at Kunde Family Vineyards near Santa Rosa, Calif. Kunde flies his drones recreationally, but he hopes one day to use them to help farmers manage their crops. (Discover Sonoma County Wine)
"I see a bright future. That's the reason I started my lab in the Central Valley," said Chen, who was doing research with agriculture drones at Utah State University before joining the UC Merced faculty and starting the school's mechatronics lab in 2012.
The unmanned aerial systems can be programmed to fly low over fields and stream photos and videos to a ground station, where the images can be stitched together into maps or analyzed to gauge crop health. They can also be modified to land and take soil and water samples. One day they could be used in the U.S. as precision crop-dusters.
"The application of these data drones is only limited by our imagination," Chen said.
Agriculture could be the proving ground for commercial drone applications, partly because operating in rural areas far from crowds, large airports and tall buildings alleviates privacy and safety concerns.
Many experts believe that drones could revolutionize the industry.
"I think it's going to change agriculture as we know it in North America," said Scott Shearer, a professor at Ohio State University and an expert in precision agriculture. "It's definitely going to allow producers to become much more efficient."
Shearer said drones already can be used to provide more timely crop data and higher-resolution aerial imagery at a fraction of the cost of using traditional piloted aircraft or satellites.
"It's a bit of a game changer," Shearer said.
A 2013 study by a drone trade group estimated that future commercial drone markets would be largely in agriculture, with some in public safety such as law enforcement, firefighting and emergency management.
The study, by the Assn. for Unmanned Vehicle Systems International, projected that the economic effect of integrating drones into the national airspace would top $2.3 billion in California in the first three years, more than in any other state, leading to the creation of more than 12,000 jobs in this state alone.
Some experts caution that the trade group's predictions may be too optimistic, but they acknowledge that there is a huge opportunity for agricultural drones.
The benefits of ag drones are promising for farmers growing largely commodity crops in the Midwest, but Shearer said they may be even greater for those cultivating high-value crops, such as California's wine grape growers.
Ryan Kunde, winemaker for DRNK Wines near Sebastopol, has been testing drones with the goal of one day using them to help make decisions in the vineyard — where to harvest first, what plants need more nutrients, which areas need more water and which need less.
"Small increases in productivity make a huge impact," Kunde said. "It's farming smarter."
Kunde began tinkering with drones in 2010, and eventually formed a company to provide drone monitoring data to grape growers for a fee. But until the FAA approves commercial drone use, that business is "kind of in a holding pattern," he said.
"The market is there. We just don't have the guidelines to regulate it," Kunde said.
Drone advocates say wider use depends on the complex process of integrating unmanned aircraft into national airspace, which will start to be outlined in forthcoming FAA rules.
That integration was congressionally mandated by September 2015, though a recent Transportation Department audit found that the FAA is likely to miss that deadline. The FAA has said rules governing small drones under 55 pounds that fly below 400 feet will be introduced later this year, but some industry officials cautioned that they may not take effect until 2016.
Very few commercial operators have received FAA exemptions allowing them to use drones in the U.S. Monrovia drone maker AeroVironment Inc. this year became the first to get approval for commercial use of a drone over land for its Puma AE UAS, which monitors BP Exploration Inc.'s remote Prudhoe Bay oil field in Alaska.
The company plans to offer crop monitoring services for farmers, AeroVironment spokesman Steven Gitlin said, but FAA rules are holding it back.
"We could deliver valuable information to farmers tomorrow, if the rules allowed it," Gitlin said.
Some researchers at California's public universities have received limited federal approval to fly drones as part of their research. Chen, the UC Merced professor, is using drones to develop a way to turn drone data into useful guidance that farmers can follow to boost yields.
At UC Davis, professor Ken Giles has approval to fly the 200-pound Yamaha RMAX helicopter, which has been used in Japan for more than two decades as a nimble crop-duster. Part of his research is collecting the data needed to guide future regulations on the use of such remote-controlled aerial sprayers in the U.S.
Giles, who has a pilot's license, said that unlike many of the smaller drones, which can be programmed to fly a certain path without human guidance, the RMAX is not autonomous. That, plus its limited payload capacity — it can fly for about 15 minutes at full spray before needing to be refilled — could slow its adoption for U.S. agriculture.
But the technology, he said, has the potential to deliver "a level of accountability and precision that we haven't had before."
